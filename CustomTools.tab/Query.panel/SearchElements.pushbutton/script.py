# -*- coding: utf-8 -*-
from pyrevit import DB, forms, script, revit, coreutils, HOST_APP
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Document
from pyrevit.userconfig import user_config
from pyrevit.coreutils import Timer
from customOutput import hmsTimer
from customOutput import ct_icon
from stringFormating import accents2ascii

doc = __revit__.ActiveUIDocument.Document

# if there are selected elements
if revit.get_selection():
    # instances
    el_collector = revit.get_selection()
    # types
    el_type_collector = []
    for el in el_collector:
        elementTypeId = el.GetTypeId()
        elementType = Document.GetElement(doc,elementTypeId)
        if elementType not in el_type_collector:
            el_type_collector.append(elementType)
    # all elements together
    el_union = list(el_collector) + el_type_collector
# if there is no selection get all elements in the model
else:
    # instances
    el_collector = FilteredElementCollector(doc).WhereElementIsNotElementType()
    # types
    el_type_collector = FilteredElementCollector(doc).WhereElementIsElementType()
    # all elements together
    el_union = el_collector.UnionWith(el_type_collector).ToElements()



# /////// UI WINDOW /////////
class searchWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        forms.WPFWindow.__init__(self, xaml_file_name)
        # creating sections in pyRevit_config.ini
        try:
            user_config.add_section('SearchElements')
        except:
            pass
        # reading parameter values from pyRevit_config.ini and setting text box values in GUI
        try:
            self.param_value_tb.Text = user_config.SearchElements.searchParamValue
            self.param_name_tb.Text = user_config.SearchElements.searchParamName
        except:
            pass

    def process_text(self, sender, args):
        self.Close()
        contains_RB = self.Contains.IsChecked
        equals_RB = self.Equals.IsChecked
        lower_RB = self.Lower.IsChecked
        greater_RB = self.Greater.IsChecked
        find_pure = self.param_value_tb.Text
        parameterName = self.param_name_tb.Text

        # storing values to config file
        user_config.SearchElements.searchParamValue = find_pure
        user_config.SearchElements.searchParamName = parameterName

        timer = Timer()
        output = script.get_output()
        output.set_width(400)
        # changing icon
        ct_icon(output)
        output.freeze()
        
        if " OR " in find_pure:
            find_pure_list = find_pure.split(" OR ")
            count = 0
            for i in find_pure_list:
                i = find_pure[count].lower()
                count += 1
        # find = find_pure.lower()
        else:
            find_pure_list = [find_pure.lower()]
        
        # getting the element data
        schedule_data = []
        ids = []

        for el in el_union:
            if el:
                el_id = el.Id
                # check whether parameter exists
                parameter = el.LookupParameter(parameterName)
                if parameter:
                    # find_float = float(find)
                    if parameter.StorageType == DB.StorageType.Double or parameter.StorageType == DB.StorageType.Integer:
                    # print(find_float)
                        try:
                            find_float = float(find_pure_list[0])
                            parameter = el.LookupParameter(parameterName)
                            # el_param = round(el.LookupParameter(parameterName).AsDouble() * 304.8)
                            value_string = parameter.AsValueString()
                            # print(value_string)
                            try:
                                el_param = float(value_string)
                            except:
                                try:
                                    if value_string:
                                        # replacing semicolon for dot in a string
                                        s = value_string.replace(",",".").split(" ")
                                        el_param = float(s[0])
                                    else:
                                        el_param = ""
                                except ValueError:
                                    # yes no parameter hidden as an integer
                                    el_param = parameter.AsInteger()

                            paramList = [
                                output.linkify(el_id),
                                el_param,
                                el.Category.Name,
                                ]
                            # print(paramList)
                            if contains_RB or equals_RB:
                                for value in find_pure_list:
                                    if el_param == float(value):
                                        schedule_data.append(paramList)
                                        ids.append(el_id)
                                        # print(paramList)
                            elif lower_RB:
                                if el_param <= find_float:
                                    schedule_data.append(paramList)
                                    ids.append(el_id)
                            elif greater_RB:
                                if el_param >= find_float:
                                    schedule_data.append(paramList)
                                    ids.append(el_id)
                        except:
                            pass
                    # string
                    elif parameter.StorageType == DB.StorageType.String:
                        try:
                            el_param = parameter.AsString()
                            paramList = [
                                output.linkify(el_id),
                                el_param,
                                el.Category.Name,
                                ]
                            # parameter value without accents and lowercase
                            p_val_noacc_lower = accents2ascii(el_param.lower())
                            if contains_RB or lower_RB or greater_RB:
                                for value in find_pure_list:
                                    if value in p_val_noacc_lower:
                                        schedule_data.append(paramList)
                                        ids.append(el_id)
                            elif equals_RB:
                                for value in find_pure_list:
                                    if value == p_val_noacc_lower:
                                        schedule_data.append(paramList)
                                        ids.append(el_id)
                        except:
                            pass

        sorted_schedule_data = sorted(schedule_data, key=lambda x: x[2].lower())
        # printing the schedule if there are data
        if sorted_schedule_data:
            # Heading
            output.print_md("## Search Results")
            # visual divider for each category - heading, <hr> line
            category = ""
            for line in sorted_schedule_data:
                # for each new category or category with <> removed
                if category != line[2] and category != line[2][1:-1]:
                    # if category has <> in its name it causes erros in html
                    if line[2][0] == "<":
                        category = line[2][1:-1]
                    else:
                        category = line[2]
                    print(coreutils.prepare_html_str("<hr>"))
                    output.print_md("### " + category)
                newline= line[0] + " \t " + coreutils.prepare_html_str("<b>" + str(line[1]) + "</b>") + " \t " + coreutils.prepare_html_str('<span style="font-size: 0.9em; color: #51b8e1":>' + category + '</span>') + " \t "
                print(newline)
            print(coreutils.prepare_html_str("<hr>"))
        # if there are no data print status claim
        else:
            print("There is no such an Element.")
        # for timing------
        endtime = timer.get_time()
        print(hmsTimer(endtime))
        output.unfreeze()


        if ids and len(ids) < 50:
            print('{}'.format(output.linkify(ids, title='All Elements')))

        try:
            selection = revit.get_selection()
            selection.set_to(ids)
        except:
            print("selection crashed")

searchWindow('searchWindow.xaml').ShowDialog()