# -*- coding: utf-8 -*-
__title__ = 'Schody'
__doc__ = """STN 73 4130 Schodiska a sikme rampy"""

from pyrevit import script


__context__ = 'zero-doc'

url = "https://www.dropbox.com/s/y3uely4rr24gpqq/STN_734130%20Schodistia%20a%20sikme%20rampy.pdf?dl=0"
# script.open_url(url)

# from Autodesk.Revit.UI import UIApplication, RevitCommandId, PostableCommand
# doc = __revit__.ActiveUIDocument.Document
# Command_ID=RevitCommandId.LookupPostableCommandId(PostableCommand.Close)
# uiapp = UIApplication(doc.Application)

# from Autodesk.Revit.ApplicationServices import Application
# a = Application
# b = a.Dispose()

import subprocess
subprocess.run(['TASKKILL', '/F', '/IM', 'Revit.exe'], check=True)