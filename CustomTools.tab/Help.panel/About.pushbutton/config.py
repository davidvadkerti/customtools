# -*- coding: utf-8 -*- 
__doc__ = 'Opens mass message file.'

import subprocess
from pyrevit import script
from customOutput import mass_message_url

output = script.get_output()
subprocess.Popen(r'explorer /select,' + mass_message_url(output))
# subprocess.Popen(r'explorer /select,"L:\_i\CTmassMessage\doNotErase-pointer"')