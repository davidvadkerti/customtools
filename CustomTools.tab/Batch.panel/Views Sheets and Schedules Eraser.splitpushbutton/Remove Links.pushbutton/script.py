from pyrevit import revit, DB, script, forms
from pyrevit import forms
from pyrevit import revit, DB, UI
from pyrevit import script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, ImportInstance

doc = __revit__.ActiveUIDocument.Document

class removeLinks(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        forms.WPFWindow.__init__(self, xaml_file_name)


    def link_remover(self, sender, args):
        self.Close()
        CAD_Links= self.CAD_Links.IsChecked
        RVT_Links= self.RVT_Links.IsChecked
        PDF_Links= self.PDF_Links.IsChecked
        Images= self.Images.IsChecked

        cadlinks = FilteredElementCollector(doc).OfClass(DB.CADLinkType).ToElements()
        images = FilteredElementCollector(doc).OfClass(DB.ImageType).ToElements()
        rvt_links = FilteredElementCollector(doc).OfClass(DB.RevitLinkType).ToElements()
        pdf_links = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_RasterImages).ToElements()
        # ifc_links = FilteredElementCollector(doc).OfClass(DB.IfcLink).ToElements()

        with revit.Transaction("Remove Links"):
            if CAD_Links == True:
                for cad in cadlinks:
                    try:
                        revit.doc.Delete(cad.Id)
                    except:
                        pass
            if Images == True:
                for img in images:
                    try:
                        revit.doc.Delete(img.Id)
                    except:
                        pass
            # RVT and IFC links
            if RVT_Links == True:
                for rvt in rvt_links:
                    try:
                        revit.doc.Delete(rvt.Id)
                    except:
                        pass
            if PDF_Links == True:
                for pdf in pdf_links:
                    try:
                        revit.doc.Delete(pdf.Id)
                    except:
                        pass

removeLinks('removeLinksWindow.xaml').ShowDialog()