from pyrevit import revit, DB, script, forms

# disclaimer warning
if forms.alert(msg="Do you want to ungroup all Groups and remove all group types from the model?",\
               ok=True, cancel=True,\
               warn_icon=True):
    # group instances
    group_collector = DB.FilteredElementCollector(revit.doc) \
        .OfClass(DB.Group) \
        .WhereElementIsNotElementType() \
        .ToElements()
    # group types
    group_type_collector = DB.FilteredElementCollector(revit.doc) \
        .OfClass(DB.GroupType) \
        .WhereElementIsElementType() \
        .ToElements()

    output = script.get_output()

    if group_type_collector:
        if group_collector:
            with revit.Transaction ("Explode All", revit.doc):
                for group in group_collector:
                    ungrouped = group.UngroupMembers()
                for group_type in group_type_collector:
                    try:
                        revit.doc.Delete(group_type.Id)
                    except:
                        pass
        else:
            print("No Group instances in the project")
    else:
        print("No Group types in the project")