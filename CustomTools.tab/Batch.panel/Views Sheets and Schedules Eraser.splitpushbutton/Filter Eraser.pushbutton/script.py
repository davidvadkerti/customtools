from pyrevit import forms
from pyrevit import revit, DB
from pyrevit import script

# dialog
dialog = forms.alert("Do you want to delete all filters in the model?",
                  ok=False, yes=True, no=True)
if dialog:
    filters = DB.FilteredElementCollector(revit.doc)\
                .OfClass(DB.ParameterFilterElement)\
                .ToElements()

    allFilters = set()
    for flt in filters:
        allFilters.add(flt.Id)

    if not allFilters:
        forms.alert('There are no filters in the model.')
        script.exit()
    else:
        with revit.Transaction('FilterEraser'):
            for vf in allFilters:
                revit.doc.Delete(vf)