from pyrevit import revit, DB, script, forms
from pyrevit import forms
from pyrevit import revit, DB, UI
from pyrevit import script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, ImportInstance

doc = __revit__.ActiveUIDocument.Document

cat_list = []
cat_name_list = []
elements = FilteredElementCollector(doc).WhereElementIsNotElementType().ToElements()
for el in elements:
    if el.Category:
        category = el.Category
        category_name = category.Name
        if category_name not in cat_name_list:
            cat_list.append(category)
            cat_name_list.append(category_name)
# print(cat_list)
# print(cat_name_list)

sorted_cat_list = sorted(cat_list, key=lambda x: x.Name)
res = forms.SelectFromList.show(sorted_cat_list,
                                multiselect=True,
                                name_attr='Name',
                                button_name='Select Categories to Remove')

# disclaimer warning
if res and forms.alert(msg="Do you want to remove all elements of selected Categories?",\
           ok=True, cancel=True,\
           warn_icon=True):
    with revit.Transaction("Delete Categories"):
        for category in res:
            category_id = category.Id.IntegerValue
            built_in_category = BuiltInCategory(category_id)
            elementIds_to_delete = FilteredElementCollector(doc).OfCategory(built_in_category).WhereElementIsNotElementType().ToElementIds()
            # print(a)
            for el in elementIds_to_delete:
                try:
                    revit.doc.Delete(el)
                except:
                    pass