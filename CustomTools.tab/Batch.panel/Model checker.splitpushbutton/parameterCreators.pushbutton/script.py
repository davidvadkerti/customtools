# Import necessary modules from pyRevit and Revit API
from pyrevit import output, script
from pyrevit import revit, DB
from pyrevit.forms import ProgressBar
from pyrevit.coreutils import Timer
from customOutput import hmsTimer
from customOutput import file_name_getter, ct_icon

# Get the current document
doc = revit.doc
output = script.get_output()
# changing icon
ct_icon(output)
timer = Timer()

# Dictionary to store parameter names and their creators
shared_param_data = []
other_param_data = []
param_categories = {}

# Function to get the creator of an element
def get_element_creator(element):
    try:
        # Get Worksharing information (works if the project uses worksharing)
        worksharing_info = DB.WorksharingUtils.GetWorksharingTooltipInfo(doc, element.Id)
        creator = worksharing_info.Creator
        return creator
    except:
        return None

bindings = doc.ParameterBindings
it = bindings.ForwardIterator()
it.Reset()
# Iterate through the binding map to find instance bindings
while it.MoveNext():
    param_def = it.Key # Parameter definition
    binding = it.Current # Binding object

    # Check if the binding is an InstanceBinding
    if isinstance(binding, DB.InstanceBinding):
            param_name = param_def.Name
            category_list = [cat.Name for cat in binding.Categories]            
            param_categories[param_def.Id] = category_list

# Collect all elements in the project
collector_inst = DB.FilteredElementCollector(doc).WhereElementIsNotElementType()
collector_types = DB.FilteredElementCollector(doc).WhereElementIsElementType()
collector = collector_inst.UnionWith(collector_types)

collector_count = collector.GetElementCount()

# with ProgressBar(title='Processing parameters of all elements ({value} of {max_value})', cancellable=True) as pb:
with ProgressBar(step = 10000, cancellable=True) as pb:
    counter = 0
    breaked = False

    # Loop through the collected elements
    for element in collector:
        if pb.cancelled:
            breaked = True
            break
        else:
            # update progressbar
            pb.update_progress(counter, collector_count)
        counter += 1

        # Get element parameters
        params = element.Parameters
        if not breaked:
            # Loop through parameters and store their names and creators
            for param in params:
                if param.IsShared:
                    try:
                        param_def = param.Definition
                        param_name = param.Definition.Name
                        creator = get_element_creator(param)
                        shared = param.IsShared
                        storage_type = param.StorageType
                        guid = param.GUID
                        categories = param_categories[param_def.Id]
                        
                        if creator:
                            # Add the parameter and its creator to the dictionary
                            paramList = [param_name, creator, storage_type, guid, ", ".join(categories)]
                            if paramList not in shared_param_data:
                                if shared:
                                    shared_param_data.append(paramList)
                                # else:
                                    # other_param_data.append(paramList)
                    except KeyError:
                        pass
    if shared_param_data:
        sortedSharedParams = sorted(shared_param_data, key=lambda x: x[0].lower())
        output.print_table(table_data=sortedSharedParams,
                           title = "Shared Parameters - " + file_name_getter(doc),
                           columns=["Parameter Name", "Creator", "Data type", "GUID", "Categories"],
                           formats=['', '', '', '', ''])
    else:
        print("There are no Shared parameters in the Project.")

    # for timing------
    endtime = timer.get_time()
    print(hmsTimer(endtime))