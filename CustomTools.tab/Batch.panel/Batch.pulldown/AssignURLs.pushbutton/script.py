# -*- coding: utf-8 -*-
__title__ = 'Assign URLs'
__doc__ = """Assigns URLs from a list according to specific parameter values."""

# for timing------
from pyrevit.coreutils import Timer
from pyrevit import forms
from custom_output import hmsTimer
# timer = Timer()
# ----------------

from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, Transaction

# uidoc = __revit__.ActiveUIDocument
doc = __revit__.ActiveUIDocument.Document

class AssignURLsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        forms.WPFWindow.__init__(self, xaml_file_name)

    def assign(self, sender, args):
        self.Close()
        url_string = str(self.urltb.Text)
        code_param_name = str(self.codeparamtb.Text)
        url_param_name = str(self.urlparamtb.Text)
        delimiter =  str(self.delimtb.Text)
        delim_pos =  int(self.delimpostb.Text)

        urls = url_string.split("\n")
        ml_codes = []

        for u in urls:
            # if element code is last
            if delim_pos == -1:
                # last occurence of delimiter and removing file extension
                ml_code_ext = u.split(delimiter)[-1]
                ml_code = ml_code_ext[:ml_code_ext.rindex(".")]
            # negative indexes - from the end
            elif delim_pos <= 0:
                ml_code = u.split(delimiter)[delim_pos]
                # ml_code = u.split(delimiter)[-2] # if element code is one before last
            # positive indexes
            else:
                ml_code = u.split(delimiter)[delim_pos - 1]
            ml_codes.append(ml_code)
    
        elems = FilteredElementCollector(doc).WhereElementIsNotElementType().ToElements()
        out_codes = []
        out_elems = []
        out_urls = []
        for el in elems:
            parameter = el.LookupParameter("Element code")
            if parameter:
                element_code = parameter.AsString()
                if element_code:
                    # if exact code is found
                    if element_code in ml_codes:
                        index = ml_codes.index(element_code)
                        url = urls[index]
                        out_urls.append(url)
                        out_codes.append(element_code)
                        out_elems.append(el)
                    else:
                        try:
                            last_dot_index = element_code.rindex(".")
                            s_element_code = element_code[:last_dot_index]
                            # if code before the last dot XX.1 for XX.1.5 or XX.1.6
                            if  s_element_code in ml_codes:
                                index = ml_codes.index(s_element_code)
                                url = urls[index]
                                out_urls.append(url)
                                out_codes.append(s_element_code)
                                out_elems.append(el)
                        # dot in element code not found
                        except ValueError:
                            pass

        # not found element codes
        for ml_code in ml_codes:
            if ml_code not in out_codes:
                print(ml_code + " not found")

        t = Transaction(doc, "Assign URLs")
        t.Start()
        
        # for timing
        # timer = Timer()
        count = 0
        for element in out_elems:
            # try:
            # parameter = el.LookupParameter(param_name)
            code_param = element.LookupParameter(code_param_name)
            url_param = element.LookupParameter(url_param_name)
            if not code_param.IsReadOnly:
                url_param.Set(out_urls[count])
            # except AttributeError:
            #     pass
            count += 1
            
        t.Commit()
        # for timing------
        # endtime = timer.get_time()
        # print(hmsTimer(endtime))
		# ----------------  

AssignURLsWindow('AssignURLsWindow.xaml').ShowDialog()