#!/usr/bin/env python
# -*- coding: utf-8 -*-
__beta__ = True

__title__ = 'Výška a označenie\nprierazov SKP'
__doc__ = """Sets distance of Sill Height of Windows from Project zero to parameter "SH prierazu".
Sets Level number to parameter "Element floor" to Windows and Generic Models
Sets Level elevation to parameter "ref od 0" to Windows.
Sets Mark to Windows and Generic Models in xxx format f.e. 001
Sets Element code to Windows and Generic Models in SU.level.xxx format f.e. SU.5.001

Only Windows and Generic Models with Type Comments == "stavebne upravy" are processed.
You need to add theese Shared Parameters: Element floor, Element code, Poschodie, SH prierazu, Offset Shared, Priemer, ref od 0."""

__helpurl__ = "https://youtu.be/2LBi9p3gPiY"

# for timing------
from pyrevit.coreutils import Timer
from pyrevit import coreutils, forms
from customOutput import hmsTimer, ct_icon
timer = Timer()
# ----------------

from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, \
                Transaction, Document, BuiltInParameter
from pyrevit import revit, DB
from pyrevit import script

doc = __revit__.ActiveUIDocument.Document


# /////// UI WINDOW /////////
class vyskaPrierazovWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
            forms.WPFWindow.__init__(self, xaml_file_name)

    def process_opngs(self, sender, args):
        self.Close()
        allopngsRB = self.allopngs.IsChecked
        emptyopngs = self.emptyopngs.IsChecked

        # dialog
        if allopngsRB:
            dialog = forms.alert("POZOR!\nChceš naozaj prepísať všetky prierazy? ",
                          ok=False, yes=True, no=True)
        else:
            dialog = True

        if dialog:
            # /////// COLLECTORS /////////
            # collecting windows
            windows_collector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Windows) \
                .WhereElementIsNotElementType() \
                .ToElements()

            # collecting generic models
            genericModel_collector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_GenericModel) \
                .WhereElementIsNotElementType() \
                .ToElements()

            output = script.get_output()
            # changing icon
            ct_icon(output)

            # /////// GLOBAL VARIABLES /////////
            unique_types = [] # unique specifications of openings (e.g. ['-2', 'ZT', ['wc', 350, '30']])
            list_starts = [] # list of specialist + level (e.g. VZ01), numbering sequence starts here over and over
            sqc_number_list = [] # list of just last used numbers for each specialist + level
            unique_types_nums = [] # list of all numbers for each opening type
            level_starts = [] # list of last level numbers (e.g. SU.01.023), numbering sequence starts here over and over
            level_list = [] # list of levels

            # /////// FUNCTIONS /////////
            #converts feets to milimeters and makes rounded integer from the result
            def feet2mm(a):
                b = a/0.00328084
                return int(round(b))

            # highlights text using html string with css
            def text_highligter(a):
                    content = str(a)
                    html_code = "<p class='elementlink'>"+content+"</p>"
                    return coreutils.prepare_html_str(html_code)

            # error handling
            def skipped(element,parameterName):
                # notprocessed.append(element.Id.IntegerValue)
                # notprocessed.append(element.Id)
                elemID = element.Id
                elemName = element.Name
                paramList = [output.linkify(elemID), elemName]
                # paramList = [elemID, elemName]
                notprocessed.append(paramList)

                parameter = element.LookupParameter(parameterName)
                if parameter:
                    parameter.Set("no data")

            # adding leading zeros to one digit numbers
            def zerosNum(a):
                if a < 10 and a > 0:
                    a = "00"+str(a)
                elif a < 100:
                    a = "0"+str(a)
                return str(a)

            def elementFloorSetter(element,levelElement):
                elementTypeId = element.GetTypeId()
                elementType = Document.GetElement(doc,elementTypeId)
                # filtering elements
                # filterp = elementType.LookupParameter('Type Comments')
                filterp = elementType.get_Parameter(DB.BuiltInParameter.ALL_MODEL_TYPE_COMMENTS)
                if filterp and filterp.AsString() == "stavebne upravy":
                    try:
                        # setting parameters
                        # getting the levelCode string from Levels
                        levelCode = levelElement.LookupParameter("Poschodie").AsString()
                        # setting parameter "Element floor" for openings
                        levelNameP = element.LookupParameter("Element floor")
                        if levelNameP:
                                levelNameP.Set(levelCode)
                                return levelCode
                        else:
                            skipped(element,"Element floor")
                            return "N/A"
                    except:
                        skipped(element,"Element floor")
                        return "N/A"

            # numbers openings by type, every new type will have new number
            # sets dimension parameter value according to real dimensions of the element
            # atributes names:
            # circular: diameter name, circular mark f.e. "wc"
            # rectangular: width, height or depth, rectangular mark f.e. "wr"
            def markSetter(element,DiameterName,CircularMark,WidthName,HeightDepthName,RectMark,level_code):
            # # filter by Type Comments == stavebne upravy
                elementTypeId = element.GetTypeId()
                elementType = Document.GetElement(doc,elementTypeId)
                # filterp = elementType.LookupParameter('Type Comments')
                filterp = elementType.get_Parameter(DB.BuiltInParameter.ALL_MODEL_TYPE_COMMENTS)

                if filterp and filterp.AsString() == "stavebne upravy":
                    # setting Mark values
                    try:
                        diameter_param = element.LookupParameter(DiameterName).AsDouble()
                        if diameter_param > 0:
                            # circular windows and gms
                            # print(diameter_param)
                            diameterValue = feet2mm(diameter_param)
                            # print("diameter = " +str(diameter_param))
                            fire_rating = element.LookupParameter("Fire Rating").AsString()
                            spclst = element.LookupParameter("Profesia").AsString()
                            if level_code != "N/A":
                                level = level_code
                            else:
                                level = element.LookupParameter("Element floor").AsString()
                            # wc = wall circular
                            # opening specs
                            opng = [level, spclst, [CircularMark, diameterValue, fire_rating]]
                            # seting dimension parameter "Rozmer", e.g. "Ø400"
                            dim_param = element.LookupParameter("Rozmer")
                            dim_param_value = "Ø" + str(diameterValue)
                            dim_param.Set(dim_param_value)
                        else:
                            # rectangular windows and gms
                            width_param = element.LookupParameter(WidthName).AsDouble()
                            heightDepth_param = element.LookupParameter(HeightDepthName).AsDouble()
                            widthValue = feet2mm(width_param)
                            heightDepthValue = feet2mm(heightDepth_param)
                            fire_rating = element.LookupParameter("Fire Rating").AsString()
                            spclst = element.LookupParameter("Profesia").AsString()
                            if level_code:
                                level = level_code
                            else:
                                level = element.LookupParameter("Element floor").AsString()

                            # windows with Depth not equal with Wall width - niky
                            # not through the wall
                            try:
                                DepthName = "Hlbka"
                                depth_param = element.LookupParameter(DepthName).AsDouble()
                                depthValue = feet2mm(depth_param)
                                print(depthValue)

                                # RectMark >>> wr = wall rectangular
                                # opening specs
                                opng = [level, spclst, [RectMark, widthValue, heightDepthValue, fire_rating, depthValue]]
                                # seting dimension parameter "Rozmer", e.g. 400x700
                                dim_param = element.LookupParameter("Rozmer")
                                dim_param_value = str(widthValue) + "x" + str(heightDepthValue) + "x" + str(depthValue)
                                dim_param.Set(dim_param_value)

                            # standard rectangular windows - through the wall
                            except:    
                                # RectMark >>> wr = wall rectengular
                                # opening specs
                                opng = [level, spclst, [RectMark, widthValue, heightDepthValue, fire_rating]]
                                # seting dimension parameter "Rozmer", e.g. 400x700
                                dim_param = element.LookupParameter("Rozmer")
                                dim_param_value = str(widthValue) + "x" + str(heightDepthValue) 
                                dim_param.Set(dim_param_value)

                        # sorting and numbering openings
                        # prefix category + level, e.g. SU.01.
                        # prefix = "SU." + opng[0] + "."
                        if level not in level_list:
                            level_list.append(level)
                            opng_num = 1
                            level_starts.append(opng_num)
                            # print("first")
                        else:
                            index = level_list.index(level)
                            level_starts[index] += 1 
                            opng_num = level_starts[index]
                            # print("notfirst")
                        
                        opng_id = zerosNum(opng_num)
                        opng_mark = "SU." + level + "." + opng_id
                        # opng_mark = spclst + "." + level + "." + opng_id

                        # setting the parameter values - just numbers
                        # opng_num_param = element.LookupParameter("Cislo Prierazu")
                        # setting the parameter values - all opening id string
                        opng_mark_param = element.LookupParameter("Element code")
                        # processing all openings if radiobutton checked
                        if allopngsRB:
                            # opng_num_param.Set(opng_id)
                            opng_mark_param.Set(opng_mark)
                         # processing just openings with empty parameter if radio button checked
                        elif emptyopngs:
                            if opng_mark_param.AsString() == "":
                                # opng_num_param.Set(opng_id)
                                opng_mark_param.Set(opng_mark)
                    except:
                        pass

            # getting levels from generic models and other elements
            def GetLevel(item):
                #if hasattr(item, "LevelId"): return item.Document.GetElement(item.LevelId)
                if hasattr(item, "Level"): return item.Level
                elif hasattr(item, "GenLevel"): return item.GenLevel
                else:
                    try: return item.Document.GetElement(item.get_Parameter(BuiltInParameter.INSTANCE_REFERENCE_LEVEL_PARAM).AsElementId())
                    except: 
                        try:
                            return item.Document.GetElement(item.get_Parameter(BuiltInParameter.INSTANCE_SCHEDULE_ONLY_LEVEL_PARAM).AsElementId())
                        except:
                            pass

            t = Transaction(doc, "Vyska a oznacenie prierazov SKP")
            t.Start()

            notprocessed = []
            # creating lists - just for debugging
            # sill_height_values_list=[]
            # sill_height_values_list=[]
            # level_ids_list=[]
            # level_elevation_list=[]
            # sill_height_zeroMM_list=[]
            # offset_shared_values_list=[]

            # creating lists - just for debugging
            # level_ids_list=[]
            # level_name_list=[]
            # indexlist=[]

            # /////// WINDOW SILL HEIGHT /////////
            # getting window parameters
            for window in windows_collector:
                # filter by Type Comments == stavebne upravy
                elementTypeId = window.GetTypeId()
                elementType = Document.GetElement(doc,elementTypeId)
                # filterp = elementType.LookupParameter('Type Comments')
                filterp = elementType.get_Parameter(DB.BuiltInParameter.ALL_MODEL_TYPE_COMMENTS)

                if filterp and filterp.AsString() == "stavebne upravy":
                    try:
                        try:
                            # hosted elements with Sill Height
                            # geting Sill Height from windows
                            # sillHeightFt = window.LookupParameter('Sill Height')
                            sillHeightFt = window.get_Parameter(DB.BuiltInParameter.INSTANCE_SILL_HEIGHT_PARAM)
                            sill_height_value = sillHeightFt.AsDouble()
                            # sill_height_values_list.append(feet2mm(sill_height_value))

                        except:
                            # nonhosted elements with Offset Shared
                            # geting Offset Shared from windows
                            OffsetSharedFt = window.LookupParameter('Offset Shared')
                            offset_shared_value = OffsetSharedFt.AsDouble()
                            # offset_shared_values_list.append(feet2mm(offset_shared_value))

                        # geting levels from windows
                        level = window.LevelId
                        # level_ids_list.append(level)

                        # getting elevation of levels from project zero +0.000
                        level_element = Document.GetElement(doc,level)
                        level_elevation = level_element.Elevation
                        # level_elevation_list.append(feet2mm(level_elevation))

                        # write Level Height Values to "ref od 0" parameter of windows
                        level_height_param = window.LookupParameter('ref od 0')
                        if level_height_param:
                                level_height_param.Set(level_elevation)

                        # add Level Height Values to Sill Height Values of windows
                        try:
                            sill_height_zero = level_elevation + sill_height_value
                            # sill_height_zeroMM = round(feet2mm(sill_height_zero))
                            # sill_height_zeroMM_list.append(sill_height_zeroMM)
                        except:
                            sill_height_zero = level_elevation + offset_shared_value

                        # writing "SH prierazu" parameters
                        custom_param = window.LookupParameter('SH prierazu')
                        if custom_param:
                            custom_param.Set(sill_height_zero)

                    except:
                        # notprocessed.append(window.Id.IntegerValue)
                        # custom_param = window.LookupParameter('SH prierazu')
                        skipped(window,'SH prierazu')

            # /////// POSCHODIE PARAMETER ASIGNMENT /////////
            # for window in windows_collector:

            # for element in genericModel_collector:


            # /////// UNIQUE MARK PARAMETER ASIGNMENT /////////
            # Types of openings used in markSetter function, a is used as Mark sequence
            elementTypes = []
            a = 0
            for window in windows_collector:
                try:
                    level = window.LevelId
                    levelElement = Document.GetElement(doc,level)
                    # setting parameters
                    level_code = elementFloorSetter(window,levelElement)
                except:
                    skipped(window,"Element floor")
                    level_code = "N/A"
                # setting Mark values - atributes are parameters names, wc = window circular
                markSetter(window,"Priemer","wc","Width","Height","wr", level_code)

            for element in genericModel_collector:
                try:
                    # Workplane based elements
                    try:
                        levelElement = GetLevel(element)
                    # facebased elements
                    except:
                        level = element.LevelId
                        levelElement = Document.GetElement(doc,level)
                #   setting parameters
                    level_code = elementFloorSetter(element,levelElement)
                except:
                    skipped(element,"Element floor")
                    level_code = "N/A"
                # setting Mark values - atributes are parameters names, gmc = generic model circular
                markSetter(element,"Diameter","gmc","Width","Depth","gmr", level_code)

            t.Commit()

            # just for debuging window sill height
            # print("control lists for debugging")
            # print(sill_height_values_list)
            # print(level_ids_list)
            # print(level_elevation_list)
            # print(sill_height_zeroMM_list)
            # print(offset_shared_values_list)

            # debuging poschodie
            # print(level_name_list)
            # print(indexlist)

            # Final Claims

            countOfSkippedElms = len(notprocessed)
            print(text_highligter(str(countOfSkippedElms) +" elements were skipped"))
            # print(notprocessed)
            # if countOfSkippedElms>0:
            #     output.print_table(table_data = notprocessed,
            #                        title = "Skipped elements",
            #                        columns=["Element Id", "Name"],
            #                        formats=['', ''])

            # for timing------
            endtime = timer.get_time()
            print(hmsTimer(endtime))
            endtimeRound = round(endtime*1000)/1000
            print("\nTime "+str(endtimeRound)+" seconds.")
            # --------------

vyskaPrierazovWindow('vyskaPrierazovWindow.xaml').ShowDialog()