# -*- coding: UTF-8 -*-

__title__ = 'Generate Units'
__doc__ = 'Generate Units (sets of rooms, appartments...) by doors and windows between rooms.'
__author__ = 'David Vadkerti'
__highlight__ = 'new'


from pyrevit import revit, DB
from pyrevit import forms

from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory
from Autodesk.Revit.DB import BuiltInParameter, Phase, Document, ElementId, Transaction
from Autodesk.Revit.UI import UIApplication
from stringFormating import zeroAdder
from modeling import rs2wallWithDoors

# /////// UI WINDOW /////////
class generateUnitsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        forms.WPFWindow.__init__(self, xaml_file_name)

    def generateUnits(self, sender, args):
        self.Close()
        # settings
        process_windowsCB = self.process_windows.IsChecked
        process_rsCB = self.process_rs.IsChecked

        # Get the active document
        doc = __revit__.ActiveUIDocument.Document
        uiapp = UIApplication(doc.Application)

        # room separation conversion
        # creates walls and roors from room separations
        # there is a transaction inside
        # returns the list of walls
        if process_rsCB == 1:
            tmp_walls = rs2wallWithDoors()

        # Get the current phase (you might need to specify which phase you want)
        phases = list(FilteredElementCollector(doc).OfClass(Phase))
        current_phase = phases[-1]  # Assuming the last phase is the most recent

        if process_windowsCB == 1:
            # Collect doors
            doors_collector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Doors)

            # Collect windows
            window_collector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Windows)

            el_collector= doors_collector.UnionWith(window_collector). \
                        WhereElementIsNotElementType().ToElements()
        else:
            # Collect doors
            el_collector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Doors). \
                        WhereElementIsNotElementType().ToElements()

        rooms_ent_from = []
        rooms_ent_to = []
        rooms_from = []
        rooms_to = []

        for el in el_collector:
            # print(el.Id.IntegerValue)
            # Get the el type
            elementTypeId = el.GetTypeId()
            elementType = doc.GetElement(elementTypeId)

            # Check the 'Type Comments' parameter
            filterp = elementType.get_Parameter(BuiltInParameter.ALL_MODEL_TYPE_COMMENTS)
            el_cat_id_val = el.Category.Id.IntegerValue
            if el_cat_id_val == int(BuiltInCategory.OST_Doors) or \
                el_cat_id_val == int(BuiltInCategory.OST_Windows) and filterp.AsString() == "balkon":
                # print(filterp.AsString())
                # print(el.Category.Name)
                # print("------")
                try:
                    room_from = el.FromRoom[current_phase].Id.IntegerValue
                except:
                    room_from = "no room"
                # try:
                r = el.ToRoom[current_phase]
                if r:
                    room_to = el.ToRoom[current_phase].Id.IntegerValue
                # except:
                else:
                    room_to = "no room"
                if filterp and filterp.AsString() == "vstup":
                        rooms_ent_from.append(room_from)
                        rooms_ent_to.append(room_to)
                else:
                        rooms_from.append(room_from)
                        rooms_to.append(room_to)

        # print(rooms_ent_from)
        # print(rooms_ent_to)
        # print(rooms_from)
        # print(rooms_to)
        # print("---------")

        # Place your code below this line
        room_cnctns = list(zip(rooms_from, rooms_to))
        # print(room_cnctns)
        apt_start_list = list(zip(rooms_ent_from, rooms_ent_to))
        # print(apt_start_list)

        apts = []
        count = 0
        for cnctn in room_cnctns:
            for apt_start in apt_start_list:
                for start in apt_start:
                    if start != "no room":
                        # try:
                            if start in cnctn:
                                    if [start] not in apts:
                                        apts.append([start])
                        # except:
                        #     pass
            count += 1
        # print("apartment starts")
        # print(apts)

        for apt in apts:
            for i in range(0,3):
                for rc in room_cnctns:
                    # print("---")    
                    # print(apt)
                    # print(rc[0])
                    # print(rc[1])
                    if rc[0] != "no room" and rc[0] in apt:
                        apt.append(rc[1])
                    elif rc[1] != "no room" and rc[1] in apt:
                        apt.append(rc[0])

        # print(apts)


        # with revit.Transaction('Generate Units') as t:
        t = Transaction(doc, "Generate Units")
        t.Start()
        # dictionary level:levelcount
        level_counts = {}

        for a in apts:
            if a[0] != "no room":
                room_id = ElementId(a[0])
            else:
                if a[1] != "no room":
                    room_id = ElementId(a[1])
                # for doors or windows with no rooms at all
                else:
                    pass
            room = doc.GetElement(room_id)
            level = room.LevelId
            levelElement = Document.GetElement(doc,level)
            levelCodeParam = levelElement.LookupParameter("Poschodie")
            if levelCodeParam and levelCodeParam.AsString() is not None:
                levelCode = levelCodeParam.AsString()
            else: 
                try:
                    # everything before the first dot
                    levelCode = levelElement.Name.split(".")[0]
                except:
                    levelCode = levelElement.Name

            # create new sequence for each levelCode
            try:
                level_counts[levelCode] += 1
            except KeyError:
                level_counts[levelCode] = 1
            code = levelCode + "." + zeroAdder(level_counts[levelCode],2)
            # print(code)
            for elemID in a:
                if elemID != "no room":
                    r = doc.GetElement(ElementId(elemID))
                    parameter = r.LookupParameter("Cislo_Bytu/Priestoru")
                    if parameter:
                        parameter.Set(code)
                    else:
                        # parameter = r.LookupParameter("Department")
                        parameter = r.get_Parameter(BuiltInParameter.ROOM_DEPARTMENT)
                        parameter.Set(code)

        # removing temporary walls converted from room separation lines
        if process_rsCB == 1:
            for wall in tmp_walls:
                doc.Delete(wall.Id)

        t.Commit()


generateUnitsWindow('generateUnitsWindow.xaml').ShowDialog()