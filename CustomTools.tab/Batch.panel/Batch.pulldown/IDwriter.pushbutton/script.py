from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, \
                Transaction, Document, BuiltInParameter
from pyrevit import revit, DB

doc = __revit__.ActiveUIDocument.Document

walls = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Walls)                
floors = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Floors)
doors = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Doors)
ceilings = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Ceilings)
generic_models = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_GenericModel)
railings = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_StairsRailing)
roofs = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Roofs)
windows = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Windows)
struct_frams = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_StructuralFraming)
struct_cols = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_StructuralColumns)
mech_equip = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_MechanicalEquipment)
plumb_fixtures = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_PlumbingFixtures)
rooms = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Rooms)                


el_collector= walls.UnionWith(floors). \
            UnionWith(doors). \
            UnionWith(ceilings). \
            UnionWith(generic_models). \
            UnionWith(railings). \
            UnionWith(roofs). \
            UnionWith(windows). \
            UnionWith(struct_frams). \
            UnionWith(struct_cols). \
            UnionWith(mech_equip). \
            UnionWith(plumb_fixtures). \
            UnionWith(rooms). \
            WhereElementIsNotElementType().ToElements()

# idlist = []

with revit.Transaction('ID writer'):
    for el in el_collector:
        elem_id_param = el.LookupParameter("ctElementId")
        elem_id_param.Set(str(el.Id.IntegerValue))
        # idlist.append(el.Id.IntegerValue)

        uid_param = el.LookupParameter("ctUniqueId")
        uid_param.Set(el.UniqueId)

# print(idlist)